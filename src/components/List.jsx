function List({ taskList, deletefunc}) {
    
    return (
        taskList.length !== 0 && (
            <ul>
                {taskList.map((elem, index) => {
                    return (
                        <div className="listContainer">
                            <li key={index} id={index}>{elem.task} 
                            <br />
                            {elem.date}</li>
                            <button style={{color: "#ff0000"}} onClick={() => deletefunc(index)} >X</button>
                        </div>
                    )
                })}
            </ul>
        )
    )
}

export default List
