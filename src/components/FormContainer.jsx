import { useState } from "react"
import List from "./List"

function FormContainer() {
    const [tasks, setTasks] = useState([])
    const [curr_task, addTask] = useState({})

    function handleDelete(id) {
        setTasks((elem) => {
            console.log(elem)
            return elem.filter((val, index) => index !== id)
        })
    }

    function addByEnter(e) {
        if (e.key === "Enter") {
            setTasks([...tasks, curr_task])
        }
    }

    return (
        <>
            <section className="formContainer" onKeyDown={(e) => addByEnter(e)}>
                <h1>My Todo !</h1>
                <input
                    type="text"
                    placeholder="Enter The task"
                    onChange={(e) =>
                        addTask({ ...curr_task, task: e.target.value })
                    }
                />
                <input
                    type="date"
                    placeholder="Enter the task date"
                    onChange={(e) => {
                        if (e.target.value !== "") {
                            addTask({ ...curr_task, date: e.target.value })
                        }
                    }}
                />
                <button
                    className="Add_task"
                    onClick={() => {
                        if (Object.keys(curr_task).length !== 0) {
                            setTasks([...tasks, curr_task])
                        }
                    }}
                >
                    Add
                </button>
                <List taskList={tasks} deletefunc={handleDelete} />
            </section>
        </>
    )
}

export default FormContainer
